package hoellebauer.wtc.model

class EdamamRecipeEntity {

    data class EdamamInfo(
        val q: String,
        val from: Int,
        val to: Int,
        val more: Boolean,
        val count: Int,
        val hits: Array<Hit>
    )

    data class Hit(
        val recipe: Recipe
    )

    data class Recipe(
        val uri: String,
        val label: String,
        val image: String,
        val source: String,
        val url: String,
        val shareAs: String,
        val yield: Double,
        val dietLabels: Array<String>,
        val healthLabels: Array<String>,
        val cautions: Array<String>,
        val ingredientLines: Array<String>,
        val ingredients: Array<Ingredients>,
        val totalTime: Double,
        val calories: Double,
        val totalWeight: Double
    ) {

        var favorite: Boolean = false

        fun dietLabelStrg(): String {
            var labels = ""
            for (diet in dietLabels) {
                if (labels.isNotEmpty()) {
                    labels += ", "
                }
                labels += diet
            }
            return labels
        }

        fun healthLabelStrg(): String {
            var labels = ""
            for (health in healthLabels) {
                if (labels.isNotEmpty()) {
                    labels += ", "
                }
                labels += health
            }
            return labels
        }
    }

    data class Ingredients(
        val text: String,
        val weight: Double,
        val image: String
    )
}