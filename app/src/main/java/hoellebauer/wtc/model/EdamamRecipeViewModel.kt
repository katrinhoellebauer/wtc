package hoellebauer.wtc.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


class EdamamRecipeViewModel : ViewModel() {

    private val recipeData: MutableLiveData<EdamamRecipeEntity.EdamamInfo> =
        MutableLiveData<EdamamRecipeEntity.EdamamInfo>()

    private val suggestionChange: MutableLiveData<Boolean> = MutableLiveData()

    private val suggestedRecipe: MutableLiveData<EdamamRecipeEntity.Recipe> = MutableLiveData()

    private val selectedRecipe: MutableLiveData<EdamamRecipeEntity.Recipe> = MutableLiveData()

    fun getRecipeData(): LiveData<EdamamRecipeEntity.EdamamInfo>? {
        return recipeData
    }

    fun updateRecipeData(result: EdamamRecipeEntity.EdamamInfo?) {
        recipeData.value = result
    }

    fun getSuggestionChange(): LiveData<Boolean> {
        return suggestionChange
    }

    fun suggestionChange(change: Boolean) {
        suggestionChange.value = change
    }

    fun getSelectedRecipe(): LiveData<EdamamRecipeEntity.Recipe> {
        return selectedRecipe
    }

    fun selectRecipe(recipe: EdamamRecipeEntity.Recipe) {
        selectedRecipe.value = recipe
    }

    fun getSuggestedRecipe(): LiveData<EdamamRecipeEntity.Recipe> {
        return suggestedRecipe
    }

    fun updateSuggestion(recipe: EdamamRecipeEntity.Recipe) {
        suggestedRecipe.value = recipe
    }
}
