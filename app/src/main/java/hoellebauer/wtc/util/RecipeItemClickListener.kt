package hoellebauer.wtc.util

import android.graphics.Bitmap
import android.view.View
import hoellebauer.wtc.model.EdamamRecipeEntity

interface RecipeItemClickListener {
    fun onLikedClick(v: View, b: Bitmap)
    fun onRecipeClicked(imageView: View, recipe: EdamamRecipeEntity.Recipe)
}