package hoellebauer.wtc.activity

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import hoellebauer.wtc.R
import hoellebauer.wtc.connection.HttpConnectionCallback
import hoellebauer.wtc.connection.HttpConnectionTask
import hoellebauer.wtc.fragment.MainFragment
import hoellebauer.wtc.fragment.SplashFragment
import hoellebauer.wtc.model.EdamamRecipeEntity
import hoellebauer.wtc.model.EdamamRecipeViewModel
import java.util.*
import kotlin.math.sqrt

class MainActivity : AppCompatActivity(), HttpConnectionCallback {

    private val viewModel: EdamamRecipeViewModel by viewModels()

    private var sensorManager: SensorManager? = null
    private var acceleration = 10f
    private var currentAcceleration = SensorManager.GRAVITY_EARTH
    private var lastAcceleration = SensorManager.GRAVITY_EARTH

    private var downloadTask: HttpConnectionTask? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) { // initial transaction should be wrapped like this
            supportFragmentManager.beginTransaction()
                .replace(R.id.root_container, SplashFragment.newInstance())
                .commitAllowingStateLoss()
        }

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        Objects.requireNonNull(sensorManager)!!.registerListener(
            sensorListener,
            sensorManager!!
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
            SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM
        )
    }

    override fun onResume() {
        sensorManager?.registerListener(
            sensorListener, sensorManager!!.getDefaultSensor(
                Sensor.TYPE_ACCELEROMETER
            ), SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM
        )
        super.onResume()
    }

    override fun onPause() {
        sensorManager!!.unregisterListener(sensorListener)
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        cancelDownload()
    }

    fun startDownload(keyword: String? = "vegan") {
        cancelDownload()
        this?.also {
            downloadTask =
                HttpConnectionTask(this).apply {
                    execute(keyword)

                }
        }
    }

    private fun cancelDownload() {
        downloadTask?.cancel(true)
    }

    override fun getActiveNetworkInfo(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val nw = connectivityManager.activeNetwork ?: return false
            val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
            return when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                //for other device how are able to connect with Ethernet
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                //for check internet over Bluetooth
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH) -> true
                else -> false
            }
        } else {
            return connectivityManager.activeNetworkInfo?.isConnected ?: false
        }
    }

    override fun finishDownloading(result: EdamamRecipeEntity.EdamamInfo?) {
        //notify view model
        viewModel.updateRecipeData(result)
        //update suggestion, because old recipes removed
        viewModel.suggestionChange(true)

        supportFragmentManager.fragments.lastOrNull()?.let { currentFragment ->
            if (currentFragment !is MainFragment) {
                //splash to mainfragment
                supportFragmentManager.beginTransaction()
                    .replace(R.id.root_container, MainFragment.newInstance())
                    .commitAllowingStateLoss()
            }
        }
    }

    override fun missingConnection() {
        AlertDialog.Builder(this)
            .setTitle(R.string.no_connection)
            .setMessage(R.string.no_connection_msg)
            .setCancelable(false)
            .setNegativeButton(android.R.string.cancel) { dialogInterface: DialogInterface, _: Int ->
                //close dialog and app
                dialogInterface.dismiss()
                this.finish()
            }
            .setPositiveButton(R.string.settings) { _: DialogInterface, _: Int ->
                //open wifi settings
                this.finish()
                startActivity(Intent(Settings.ACTION_WIFI_SETTINGS))
            }
            .show()
    }

    override fun errorOccurred() {
        AlertDialog.Builder(this)
            .setTitle(R.string.error)
            .setMessage(R.string.error_msg)
            .setCancelable(false)
            .setPositiveButton(android.R.string.ok) { dialogInterface: DialogInterface, _: Int ->
                //close dialog and app
                dialogInterface.dismiss()
                this.finish()
            }
            .show()
    }

    private val sensorListener: SensorEventListener = object : SensorEventListener {
        //detecht shaking
        override fun onSensorChanged(event: SensorEvent) {
            val x = event.values[0]
            val y = event.values[1]
            val z = event.values[2]
            lastAcceleration = currentAcceleration
            currentAcceleration = sqrt((x * x + y * y + z * z).toDouble()).toFloat()
            val delta: Float = currentAcceleration - lastAcceleration
            acceleration = acceleration * 0.9f + delta

            if (acceleration > 12f) {
                //update suggestion
                viewModel.suggestionChange(true)
            }
        }

        override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {}
    }
}
