package hoellebauer.wtc.connection

import hoellebauer.wtc.model.EdamamRecipeEntity

interface HttpConnectionCallback {

    fun getActiveNetworkInfo(): Boolean

    fun missingConnection()

    fun errorOccurred()

    fun finishDownloading(result: EdamamRecipeEntity.EdamamInfo?)
}
