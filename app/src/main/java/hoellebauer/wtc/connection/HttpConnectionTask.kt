package hoellebauer.wtc.connection

import android.os.AsyncTask
import com.google.gson.Gson
import hoellebauer.wtc.model.EdamamRecipeEntity
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class HttpConnectionTask(callback: HttpConnectionCallback) :
    AsyncTask<String, Void, EdamamRecipeEntity.EdamamInfo>() {

    private var callback: HttpConnectionCallback? = null

    init {
        setCallback(callback)
    }

    private fun setCallback(callback: HttpConnectionCallback) {
        this.callback = callback
    }

    override fun onPreExecute() {
        super.onPreExecute()
        //check internet connection
        if (callback != null) {
            val networkInfo = callback?.getActiveNetworkInfo()
            if (!networkInfo!!) {
                // If no connectivity, cancel task and update Callback with null data.
                callback?.missingConnection()
                cancel(true)
            }
        }
    }

    override fun doInBackground(vararg params: String?): EdamamRecipeEntity.EdamamInfo? {

        //request recipes
        val mURL =
            URL("https://api.edamam.com/search?q=" + params[0] + "&app_id=d692521d&app_key=82076bfbc343ba02ec5f03c917e6b02e&to=30")

        try {
            with(mURL.openConnection() as HttpURLConnection) {
                // optional default is GET
                requestMethod = "GET"

                BufferedReader(InputStreamReader(inputStream)).use {
                    val response = StringBuffer()

                    var inputLine = it.readLine()
                    while (inputLine != null) {
                        response.append(inputLine)
                        inputLine = it.readLine()
                    }
                    it.close()

                    return Gson().fromJson(
                        response.toString(),
                        EdamamRecipeEntity.EdamamInfo::class.java
                    )
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            cancel(true)
            callback?.errorOccurred()
        }

        return null
    }

    override fun onPostExecute(result: EdamamRecipeEntity.EdamamInfo?) {
        callback?.apply {
            finishDownloading(result)
        }
    }
}