package hoellebauer.wtc.fragment

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.transition.TransitionInflater
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.appcompat.widget.SearchView
import androidx.core.graphics.drawable.toBitmap
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import hoellebauer.wtc.R
import hoellebauer.wtc.adapter.RecipeRecyclerViewAdapter
import hoellebauer.wtc.model.EdamamRecipeEntity
import hoellebauer.wtc.model.EdamamRecipeViewModel
import hoellebauer.wtc.util.CrossFadeTransitionFactory
import hoellebauer.wtc.util.RecipeItemClickListener
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.layout_bottom_toolbar.*
import kotlinx.android.synthetic.main.layout_suggestion.*
import kotlinx.android.synthetic.main.layout_toolbar.*


class MainFragment : Fragment(), RecipeItemClickListener {

    companion object {
        @JvmStatic
        fun newInstance() =
            MainFragment()
    }

    private val viewModel: EdamamRecipeViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sharedElementEnterTransition =
                TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rv_recipe.apply {
            layoutManager = GridLayoutManager(activity, 2)
            adapter = RecipeRecyclerViewAdapter(this@MainFragment)
        }

        viewModel.getRecipeData()?.observe(this, Observer { list ->
            // Update the list UI
            (rv_recipe.adapter as RecipeRecyclerViewAdapter).updateData(list.hits)
            Log.i(MainFragment::class.java.name, list.hits.size.toString())
        })

        viewModel.getSuggestionChange().observe(this, Observer {
            if (it) {
                //update suggestion view
                randomSuggestion()
            }
        })

        viewModel.getSuggestedRecipe().observe(this, Observer {
            updateSuggestionView(it)
        })

        toolbar.findViewById<SearchView>(R.id.menu_search_action)
            .setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    //new api request
                    //(activity as MainActivity).startDownload(query)
                    view.clearFocus()

                    activity?.supportFragmentManager?.beginTransaction()
                        ?.add(R.id.root_container, SplashFragment.newInstance(query))?.commit()
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    return true
                }
            })

        ib_like.setOnClickListener {
            if (viewModel.getSuggestedRecipe().value != null) {
                viewModel.getSuggestedRecipe().value!!.favorite = true
            }
            ib_like.setImageResource(R.drawable.ic_favorite_24px)
            onLikedClick(ll_suggestion, iv_meal_suggestion.drawable.toBitmap())
        }

        ll_suggestion.setOnClickListener {
            if (viewModel.getSuggestedRecipe().value != null) {
                val recipe = viewModel.getSuggestedRecipe().value!!
                ViewCompat.setTransitionName(it, recipe.uri)
                onRecipeClicked(it, recipe)
            }
        }
    }

    /**random index between 0 and recipes count for suggested recipe*/
    private fun randomSuggestion() {
        val count = (rv_recipe.adapter as RecipeRecyclerViewAdapter).itemCount - 1
        if (count > 0) {
            val index = (0..count).random()
            viewModel.updateSuggestion(
                (rv_recipe.adapter as RecipeRecyclerViewAdapter).getRecipeAt(
                    index
                )
            )
            viewModel.suggestionChange(false)

            //suggestion shake animation
            ll_suggestion.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake))
        }
    }

    /**update suggested recipe view*/
    private fun updateSuggestionView(recipe: EdamamRecipeEntity.Recipe) {
        Glide.with(this).load(recipe.image)
            .placeholder(R.drawable.ic_stir_small)
            .centerInside()
            .transition(DrawableTransitionOptions.with(CrossFadeTransitionFactory()))
            .into(iv_meal_suggestion)
        tv_suggestion_title.text = recipe.label
        tv_diet_label.text = recipe.dietLabelStrg()
        if (recipe.favorite) {
            ib_like.setImageResource(R.drawable.ic_favorite_24px)
        }
    }

    override fun onLikedClick(v: View, b: Bitmap) {
        //add recipe to favorite list
        animateView(v, b)
    }

    /**open detail fragment*/
    override fun onRecipeClicked(ivItem: View, recipe: EdamamRecipeEntity.Recipe) {
        viewModel.selectRecipe(recipe)

        val name = ViewCompat.getTransitionName(ivItem)
        if (name != null) {
            activity?.supportFragmentManager?.beginTransaction()
                ?.addSharedElement(ivItem, name)
                ?.replace(R.id.root_container, DetailRecipeFragment.newInstance(name))
                ?.addToBackStack(null)?.commit()
        }
    }

    /**animation - recipe added to favorite*/
    private fun animateView(recipeView: View, b: Bitmap) {
        iv_animation_dummy.setImageBitmap(b)
        iv_animation_dummy.visibility = View.VISIBLE
        val coordIBtnMyRecipes = IntArray(2)
        ib_my_recipes.getLocationInWindow(coordIBtnMyRecipes)
        val coordRecipeView = IntArray(2)
        recipeView.getLocationInWindow(coordRecipeView)
        val animSetXY = AnimatorSet()
        val x: ObjectAnimator = ObjectAnimator.ofFloat(
            iv_animation_dummy,
            "translationX",
            coordRecipeView[0].toFloat(),
            coordIBtnMyRecipes[0].toFloat()
        )
        val y: ObjectAnimator = ObjectAnimator.ofFloat(
            iv_animation_dummy,
            "translationY",
            coordRecipeView[1].toFloat(),
            coordIBtnMyRecipes[1].toFloat()
        )
        val sy = ObjectAnimator.ofFloat(iv_animation_dummy, "scaleY", 0.8f, 0f)
        val sx = ObjectAnimator.ofFloat(iv_animation_dummy, "scaleX", 0.8f, 0f)
        val alpha = ObjectAnimator.ofFloat(iv_animation_dummy, "alpha", 1f, 0f)
        animSetXY.playTogether(x, y, sx, sy, alpha)
        animSetXY.duration = 600
        animSetXY.start()
    }
}
