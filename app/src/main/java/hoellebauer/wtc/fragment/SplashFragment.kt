package hoellebauer.wtc.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import hoellebauer.wtc.R
import hoellebauer.wtc.activity.MainActivity
import hoellebauer.wtc.util.CrossFadeTransitionFactory

class SplashFragment(private val keyword: String?) : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_splash, container, false)

        //start stir gif animation
        Glide.with(this).load(R.drawable.stir)
            .placeholder(R.drawable.stir)
            .transition(DrawableTransitionOptions.with(CrossFadeTransitionFactory()))
            .into(view.findViewById(R.id.iv_stir))

        (activity as MainActivity).startDownload(keyword)

        return view
    }

    companion object {
        @JvmStatic
        fun newInstance(keyword: String? = "vegan") =
            SplashFragment(keyword)
    }
}
