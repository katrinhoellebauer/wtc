package hoellebauer.wtc.fragment


import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import hoellebauer.wtc.R
import hoellebauer.wtc.adapter.IngredientsRecyclerViewAdapter
import hoellebauer.wtc.model.EdamamRecipeViewModel
import hoellebauer.wtc.util.CrossFadeTransitionFactory
import kotlinx.android.synthetic.main.fragment_detail_recipe.*

/**
 * A simple [Fragment] subclass.
 */
class DetailRecipeFragment(private val transitionName: String) : Fragment() {

    companion object {
        @JvmStatic
        fun newInstance(transitionName: String) =
            DetailRecipeFragment(transitionName)
    }

    private val viewModel: EdamamRecipeViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sharedElementEnterTransition =
                TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_detail_recipe, container, false)

        val toolbar = view.findViewById<Toolbar>(R.id.toolbar)
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_24px)
        toolbar.setNavigationOnClickListener {
            activity?.onBackPressed()
        }
        if (toolbar.menu.findItem(R.id.menu_search_action) != null) {
            toolbar.menu.removeItem(R.id.menu_search_action)
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ll_detail.transitionName = transitionName
        }

        viewModel.getSelectedRecipe().observe(this, Observer { recipe ->
            //update view
            Glide.with(this).load(recipe.image)
                .placeholder(R.drawable.ic_stir_small)
                .centerInside()
                .transition(DrawableTransitionOptions.with(CrossFadeTransitionFactory()))
                .into(iv_meal_recipe)

            tv_suggestion_title.text = recipe.label
            tv_diet_label.text = recipe.dietLabelStrg()

            if (recipe.totalTime <= 0) {
                ll_time.visibility = View.GONE
            } else {
                tv_time.text = String.format("%.2f", recipe.totalTime)
            }

            if (recipe.calories <= 0) {
                ll_calories.visibility = View.GONE
            } else {
                tv_calories.text = getString(R.string.calories_value, recipe.calories)
            }

            if (recipe.totalWeight <= 0) {
                ll_weight.visibility = View.GONE
            } else {
                tv_weight.text = getString(R.string.weight_value, recipe.totalWeight)
            }

            tv_health_label.text = recipe.healthLabelStrg()

            //ingredients
            rv_ingredients.layoutManager = LinearLayoutManager(context)
            rv_ingredients.adapter = IngredientsRecyclerViewAdapter(recipe.ingredientLines)

            //open recipe
            btn_recipe.setOnClickListener {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(recipe.url)))
            }

            if (recipe.favorite) {
                ibtn_like.setImageResource(R.drawable.ic_favorite_24px)
            }
        })
    }
}
