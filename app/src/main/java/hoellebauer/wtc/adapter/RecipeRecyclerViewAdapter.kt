package hoellebauer.wtc.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.core.graphics.drawable.toBitmap
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import hoellebauer.wtc.R
import hoellebauer.wtc.model.EdamamRecipeEntity
import hoellebauer.wtc.util.CrossFadeTransitionFactory
import hoellebauer.wtc.util.RecipeItemClickListener

class RecipeRecyclerViewAdapter(private val recipeItemClickListener: RecipeItemClickListener) :
    RecyclerView.Adapter<RecipeRecyclerViewAdapter.RecipeViewHolder>() {

    private var hits = ArrayList<EdamamRecipeEntity.Hit>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_recipe_item, parent, false)

        return RecipeViewHolder(view)
    }

    override fun getItemCount(): Int {
        return hits.size
    }

    override fun onBindViewHolder(holder: RecipeViewHolder, position: Int) {
        val recipe = hits[position].recipe
        Glide.with(holder.ivRecipe.context)
            .load(recipe.image)
            .placeholder(R.drawable.ic_stir_small)
            .centerInside()
            .transition(DrawableTransitionOptions.with(CrossFadeTransitionFactory()))
            .into(holder.ivRecipe)
        holder.tvRecipeTitle.text = recipe.label
        holder.ibLike.setOnClickListener {
            recipe.favorite = true
            holder.ibLike.setImageResource(R.drawable.ic_favorite_24px)
            recipeItemClickListener.onLikedClick(
                holder.itemView,
                holder.ivRecipe.drawable.toBitmap()
            )
        }
        holder.itemView.setOnClickListener {
            ///set name for transition
            ViewCompat.setTransitionName(holder.itemView, recipe.uri)
            recipeItemClickListener.onRecipeClicked(holder.itemView, recipe)
        }
        if (recipe.favorite) {
            holder.ibLike.setImageResource(R.drawable.ic_favorite_24px)
        } else {
            holder.ibLike.setImageResource(R.drawable.ic_favorite_border_24px)
        }
    }

    fun getRecipeAt(index: Int): EdamamRecipeEntity.Recipe {
        return hits[index].recipe
    }

    fun updateData(data: Array<EdamamRecipeEntity.Hit>) {
        hits.clear()
        hits.addAll(data)
        notifyDataSetChanged()
    }

    class RecipeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvRecipeTitle: TextView = itemView.findViewById(R.id.tv_recipe_title)
        val ivRecipe: ImageView = itemView.findViewById(R.id.iv_recipe)
        val ibLike: ImageButton = itemView.findViewById(R.id.ib_like)
    }
}